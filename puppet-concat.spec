Name:           puppet-concat
Version:        2.2
Release:        1%{?dist}
Summary:        Puppetlabs concat module

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tgz

BuildArch:      noarch
Requires:       puppet-agent

%description
Puppetlabs concat module

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/concat/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/concat/
touch %{buildroot}/%{_datadir}/puppet/modules/concat/supporting_module

%files -n puppet-concat
%{_datadir}/puppet/modules/concat
%doc code/README.md

%changelog
* Tue Oct 15 2024 CERN Linux Droid <linux.ci@cern.ch> - 2.2-1
- Rebased to #74cd1dd1 by locmap-updater

* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> 2.1-3
- Bump release for disttag change

* Tue Feb 23 2021 Ben Morrice <ben.morrice@cern.ch> - 2.1-2
- fix requires on puppet-agent

* Mon Feb 15 2021 Ben Morrice <ben.morrice@cern.ch> - 2.1-1
- updated to 4.2.1

* Thu Jan 09 2020 Ben Morrice <ben.morrice@cern.ch> - 2.0-2
- Rebase for el8

* Thu May 03 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0-1
- Rebase for 7.5

* Tue Nov 29 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.2-1
- Rebuild for 7.3 release

* Fri Jun 17 2016 Aris Boutselis <aris.boutselis@cern.ch>
-Initial release
